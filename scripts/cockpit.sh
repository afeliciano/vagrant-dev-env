#!/bin/bash

# install cockpit

. /etc/os-release
echo "deb http://deb.debian.org/debian ${VERSION_CODENAME}-backports main"> \
/etc/apt/sources.list.d/backports.list
sudo apt-get update
sudo apt-get install -y -t ${VERSION_CODENAME}-backports cockpit