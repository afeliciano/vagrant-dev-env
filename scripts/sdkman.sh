#!/bin/bash
curl -s "https://get.sdkman.io" | bash

export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

source "/home/vagrant/.sdkman/bin/sdkman-init.sh"

sdk install java
echo -e